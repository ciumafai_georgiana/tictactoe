import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

@SuppressWarnings("serial")
public class Controller extends JComponent {
	private MouseAdapter mouseAdapter;
	private Model m;

	public Controller(Model m) {
		setPreferredSize(new Dimension(200, 200));
		this.m = m;
		this.mouseAdapter = new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				playTicTacToe(e.getX(), e.getY());
			}
		};
	}

	private void addPointUI(Integer x, Integer y, TipXO tip) {
		try {
			m.addPoint(x, y, tip);
			repaint();
		} catch (MyExceptionPoint ex) {
			ex.getMessage();
		}
	}

	public MouseAdapter getMouseAdapter() {
		return mouseAdapter;
	}

	private void playTicTacToe(int x, int y) {
		if (m.getTable().size() < 8 && !m.isMatch()) {
			addPointUI(getCoordonateFromX(x), getCoordonateFromY(y), TipXO.X);
			if (!m.isMatch())
				addComputerPoint();
		} else if (m.getTable().size() == 8 && !m.isMatch()) {
			addPointUI(getCoordonateFromX(x), getCoordonateFromY(y), TipXO.X);
		}

	}

	private void addComputerPoint() {

		int xc, yc;
		do {
			xc = getCoordonateFromX(generateRandomCoordonate());
			yc = getCoordonateFromY(generateRandomCoordonate());
		} while (m.find(xc, yc));
		addPointUI(xc, yc, TipXO.O);

	}

	private int getCoordonateFromX(int coordonate) {
		if (coordonate >= 0 && coordonate < 43) {
			return 0;
		} else if (coordonate >= 43 && coordonate < 96) {
			return 1;
		} else if (coordonate >= 96 && coordonate < 150) {
			return 2;
		} else
			return -1;
	}

	private int getCoordonateFromY(int coordonate) {
		if (coordonate >= 0 && coordonate < 50) {
			return 0;
		} else if (coordonate >= 50 && coordonate < 97) {
			return 1;
		} else if (coordonate >= 97 && coordonate < 150) {
			return 2;
		} else
			return -1;
	}

	private int generateRandomCoordonate() {
		Random random = new Random();
		return random.nextInt(150);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		draw(g);
	}

	public void clearAll() {
		m.clearAll();
	}

	private void draw(Graphics g) {
		g.drawLine(0, 50, 150, 50);
		g.drawLine(0, 97, 150, 97);
		g.drawLine(43, 0, 43, 200);
		g.drawLine(96, 0, 96, 200);
		for (Point p : m.getTable()) {
			if (p.getTip() == TipXO.X) {
				g.drawLine(p.getX() * 10 * 5, p.getY() * 10 * 5, (p.getX() * 10 + 8)* 5, (p.getY() * 10 + 8) * 5);
				g.drawLine(p.getX() * 10 * 5, (p.getY() * 10 + 8) * 5, (p.getX() * 10 + 8) * 5, p.getY() * 10 * 5);
			} else {
				g.drawOval(p.getX() * 10 * 5, p.getY() * 10 * 5, 8 * 5, 8 * 5);
			}

		}

	}

}
