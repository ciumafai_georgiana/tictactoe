public enum TipXO {
	X(1), O(-1);

	public int valoare;

	TipXO(int i) {
		this.valoare = i;
	}

	public int getValoare() {
		return valoare;
	}
}