import javax.swing.*;
import java.awt.*;
public class View extends JFrame {
	private Controller controller;
	private JPanel panel;
	private JButton rematchBtn;

	public View(Controller controller) throws Exception {
		super();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.controller = controller;
		panel = new JPanel();
		this.controller.addMouseListener(controller.getMouseAdapter());
		rematchBtn = new JButton("Rematch");
		rematchBtn.addActionListener(click -> rematch());
		this.setResizable(false);
		setSize(new Dimension(320, 220));
		setVisible(true);
	}

	private void rematch() {
		controller.clearAll();
		panel.repaint();
	}

	private void showTable() {
		panel.add(controller);
		panel.setSize(new Dimension(220, 220));
		add(panel, BorderLayout.WEST);

	}

	private void showButton() {
		add(rematchBtn, BorderLayout.EAST);
	}

	public void playMatch() {
		showTable();
		showButton();
	}

}
