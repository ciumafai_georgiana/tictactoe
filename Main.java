public class Main {
	public static void main(String[] args) {
		BoardTicTacToe table = new BoardTicTacToe();
		Model m = new Model(table);
		Controller controller = new Controller(m);
		try {
			View t = new View(controller);
			t.playMatch();
		} catch (Exception e) {
			e.getMessage();
		}
	}
}
