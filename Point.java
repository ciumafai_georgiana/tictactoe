import java.util.Objects;
public class Point {
	private Integer x;
	private Integer y;

	private TipXO tip;

	public Point(Integer x, Integer y, TipXO tip) {
		this.x = x;
		this.y = y;
		this.tip = tip;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public TipXO getTip() {
		return tip;
	}

	public void setTip(TipXO tip) {
		this.tip = tip;
	}

	public boolean equals(Object obj) {
		Point p = (Point) obj;
		return Objects.equals(this.x, p.getX()) && Objects.equals(this.y, p.getY());
	}
}
