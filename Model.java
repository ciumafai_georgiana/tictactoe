import java.util.ArrayList;
import java.util.HashMap;

public class Model {
	private BoardTicTacToe table;
	private HashMap<String, Integer> myMap;

	public Model(BoardTicTacToe table) {
		this.table = table;
		this.myMap = new HashMap<>();
	}

	private void createMap() {
		myMap.put("L1", 0);
		myMap.put("L2", 0);
		myMap.put("L3", 0);
		myMap.put("C1", 0);
		myMap.put("C2", 0);
		myMap.put("C3", 0);
		myMap.put("D1", 0);
		myMap.put("D2", 0);
	}

	public void addPoint(Integer x, Integer y, TipXO tip) throws MyExceptionPoint {
		Point point = new Point(x, y, tip);
		table.add(point);
	}

	public boolean find(int x, int y) {
		Point point = new Point(x, y, TipXO.O);
		return table.find(point);
	}

	public ArrayList<Point> getTable() {
		return table.getBoard();
	}

	public boolean isMatch() {
		createMap();
		for (Point point : table.getBoard()) {
			addToMap(point);
		}
		for (int type : myMap.values()) {
			if (type == -3 || type == 3)
				return true;
		}
		return false;
	}

	private void addToMap(Point point) {
		if (point.getX() == 0 && point.getY() == 0) {
			addFromMap("L1", point.getTip().getValoare());
			addFromMap("C1", point.getTip().getValoare());
			addFromMap("D1", point.getTip().getValoare());
		} else if (point.getX() == 1 && point.getY() == 0) {
			addFromMap("L2", point.getTip().getValoare());
			addFromMap("C1", point.getTip().getValoare());
		} else if (point.getX() == 2 && point.getY() == 0) {
			addFromMap("L3", point.getTip().getValoare());
			addFromMap("C1", point.getTip().getValoare());
			addFromMap("D2", point.getTip().getValoare());
		} else if (point.getX() == 0 && point.getY() == 1) {
			addFromMap("L1", point.getTip().getValoare());
			addFromMap("C2", point.getTip().getValoare());
		} else if (point.getX() == 1 && point.getY() == 1) {
			addFromMap("L2", point.getTip().getValoare());
			addFromMap("C2", point.getTip().getValoare());
			addFromMap("D1", point.getTip().getValoare());
			addFromMap("D2", point.getTip().getValoare());
		} else if (point.getX() == 2 && point.getY() == 1) {
			addFromMap("L3", point.getTip().getValoare());
			addFromMap("C2", point.getTip().getValoare());
		} else if (point.getX() == 0 && point.getY() == 2) {
			addFromMap("L1", point.getTip().getValoare());
			addFromMap("C3", point.getTip().getValoare());
			addFromMap("D2", point.getTip().getValoare());
		} else if (point.getX() == 1 && point.getY() == 2) {
			addFromMap("L2", point.getTip().getValoare());
			addFromMap("C3", point.getTip().getValoare());
		} else if (point.getX() == 2 && point.getY() == 2) {
			addFromMap("L3", point.getTip().getValoare());
			addFromMap("C3", point.getTip().getValoare());
			addFromMap("D1", point.getTip().getValoare());

		}
	}

	private void addFromMap(String key, Integer value) {
		myMap.put(key, myMap.get(key) + value);
	}

	public void clearAll() {
		table.clearAll();
	}
}
