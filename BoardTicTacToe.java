import java.util.ArrayList;

public class BoardTicTacToe {
	private ArrayList<Point> board;

	public BoardTicTacToe() {
		board = new ArrayList<>();
	}

	public boolean find(Point point) {
		return board.contains(point);
	}

	public void add(Point point) throws MyExceptionPoint {
		if (board.contains(point))
			throw new MyExceptionPoint("pozitie ocupata");
			board.add(point);
	}

	public ArrayList<Point> getBoard() {
		return board;
	}

	public void clearAll() {
		board.clear();
	}
}
